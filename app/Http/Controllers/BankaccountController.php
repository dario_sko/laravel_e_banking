<?php

namespace App\Http\Controllers;

use App\Models\Bankaccount;
use Illuminate\Http\Request;

class BankaccountController extends Controller
{

    public static function generateIban()
    {
        $iban = "AT6970000";
        for ($i = 0; $i < 11; $i++) {
            $iban .= random_int(0, 9);
        }
        if (BankaccountController::checkIban($iban)) {
            return $iban;
        } else {
            BankaccountController::generateIban();
        }
    }

    public static function checkIban($iban)
    {
        return Bankaccount::whereIban($iban)->exists() ? false : true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Bankaccount $bankaccount
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bankaccount $bankaccount)
    {
        $bankaccountInc = $bankaccount->id;
        $bankaccountDec = $request->user()->bankaccount->id;

        Bankaccount::where('id',$bankaccountInc)
            ->increment('balance', $request->amount);

        Bankaccount::where('id',$bankaccountDec)
            ->decrement('balance', $request->amount);

        return true;
    }

}
