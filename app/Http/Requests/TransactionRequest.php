<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'receiver' => 'required',
            'amount' => 'required|min:0',
            'description' => 'required|string|max:20',
            'reference' => 'required|string|max:20',
        ];
    }

    public function attributes()
    {
        return [
            'description' => "Verwendungszweck",
            'reference' => "Zahlungsreferenz",
        ];
    }
}
