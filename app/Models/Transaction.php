<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transactions';
    protected $primaryKey = 'id';
    protected $fillable = [
        'reference',
        'description',
        'datetime',
        'amount',
        'fk_sender',
        'fk_receiver'
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'fk_sender', 'id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'fk_receiver', 'id');
    }

    public function bankaccounts()
    {
        return collect([$this->sender, $this->receiver]);
    }
}
