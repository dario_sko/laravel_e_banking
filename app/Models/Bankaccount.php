<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bankaccount extends Model
{
    use HasFactory;

    const BIC = 'KERBERSOKI22';
    protected $table = 'bankaccounts';
    protected $primaryKey = 'id';
    protected $fillable = ['balance', 'iban', 'bic'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactionOutward()
    {
        return $this->hasMany(Transaction::class, 'fk_sender');
    }

    public function transactionInward()
    {
        return $this->hasMany(Transaction::class,'fk_receiver');
    }

    public function transactions() {
        return $this->transactionOutward->concat($this->transactionInward);

    }
}
