@extends('layouts.top')
@section('content')
    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mt-3">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Neue Transaktion</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
            </div>
        </div>
        <form action="{{ route('newTransaction') }}" method="post">
            @csrf
            @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    Die eingegebenen Daten sind fehlerhaft!
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="container border row col-12 mt-5 m-auto border-white rounded-3 bg-dark text-white shadow-lg">
                <div class="row g-3">
                    <!--Empfänger-->
                    <div class="col-sm-6">
                        <label for="inputReceiver" class="mt-3 form-label">Empfänger*</label>
                        <select class="form-select"
                                name="receiver"
                                id="inputReceiver"
                                required>
                            <option  value="" disabled selected hidden>Empfänger auswählen</option>
                            @foreach($users as $user)
                                @if(!($user->bankaccount->iban == auth()->user()->bankaccount->iban))
                                <option
                                    value="{{ $user->bankaccount->id }}">{{$user->bankaccount->iban . " - " . $user->firstname . " " . $user->lastname}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <!--Betrag-->
                    <div class="col-sm-6">
                        <label for="inputAmount" class="mt-3 form-label">Betrag*</label>
                        <input
                            type="number"
                            step="0.01"
                            name="amount"
                            class="form-control"
                            value=""
                            id="inputAmount"
                            required
                        />
                    </div>
                    <!--Verwendungszweck-->
                    <div class="col-sm-6">
                        <label for="inputDescription" class="mt-3 form-label">Verwendungszweck*</label>
                        <input
                            type="text"
                            name="description"
                            class="form-control"
                            value=""
                            id="description"
                            required
                        />
                    </div>
                    <!--Zahlungsreferenz-->
                    <div class="col-sm-6">
                        <label for="inputReference" class="mt-3 form-label">Zahlungsreferenz*</label>
                        <input
                            type="text"
                            name="reference"
                            class="form-control"
                            value=""
                            id="reference"
                            required
                        />
                    </div>

                    <!--Button-->
                    <div class="col-12 mt-3 mb-3">
                        <input type="submit" name="submit" value="Neu" class="btn btn-primary"></input>
                        <a href="{{ route('dashboard') }}" class="btn btn-secondary ms-3">Zurück</a>
                    </div>
                </div>
            </div>
        </form>
    </main>
@endsection
