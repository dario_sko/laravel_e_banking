<!--  Linke Seite -->
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block">
            <div class="position-sticky pt-3">

                <h5 class="px-3 fs-4">
                    Kundenprofil
                </h5>

                <ul class="nav flex-column px-3">
                    <li>
                        <p class="my-0 fw-bold">ID:<br></p>
                        <p class="my-0">{{  auth()->user()->id }}</p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Vorname:<br></p>
                        <p class="my-0">{{  auth()->user()->firstname }}</p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Nachname:<br></p>
                        <p class="my-0">{{  auth()->user()->lastname }}</p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Geburtstag:<br></p>
                        <p class="my-0">{{  auth()->user()->birthdate }}</p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">E-Mail:<br></p>
                        <p class="my-0">{{  auth()->user()->email }}</p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">Adresse:<br></p>
                        <p class="my-0">{{ auth()->user()->address }}</p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">PLZ:<br></p>
                        <p class="my-0">{{  auth()->user()->postalcode }}</p>
                    </li>
                </ul>

                <h5 class="px-3 fs-4 mt-3">
                    Bankkonto
                </h5>
                <!--Bankkonto Ausgabe-->


                <ul class="nav flex-column mb-2 px-3">
                    <li class="my-0">
                        <p class="my-0 fw-bold">Kontostand:<br></p>
                        <p class="my-0">{{ auth()->user()->bankaccount->balance }}€</p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">IBAN:<br></p>
                        <p class="my-0">{{ auth()->user()->bankaccount->iban }}</p>
                    </li>
                    <li class="my-0">
                        <p class="my-0 fw-bold">BIC:<br></p>
                        <p class="my-0">{{ auth()->user()->bankaccount->bic }}</p>
                    </li>
                </ul>
            </div>
        </nav>
