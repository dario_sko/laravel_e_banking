@extends('layouts.top')
@section('content')

    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mt-3">

        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Transaktionen</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group me-2">
                    {{-- Mitarbeiter Button und Neue Transaktionen --}}
                    <a href="{{ route('transaction') }}" class="btn btn-primary">Neue Transaktion</a>

                </div>
            </div>
        </div>
        <!--Transaktionen Ausgabe-->

        <div class="table-responsive">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Empfänger</th>
                    <th scope="col">Absender</th>
                    <th scope="col">Verwendungszweck</th>
                    <th scope="col">Zahlungsreferenz</th>
                    <th scope="col">Datum/Zeit</th>
                    <th scope="col" colspan="2">Betrag</th>
                </tr>
                </thead>
                <tbody>
                {{-- Tabelleninhalt Transaktionen --}}
                @foreach(auth()->user()->bankaccount->transactions()->sortBy('id') as $transaction)
                    <tr
                    class="{{\App\Models\User::find($transaction->fk_receiver)->firstname == auth()->user()->firstname ? 'table-success' : 'table-danger'}}">
                        <td>{{ $transaction->id }}</td>
                        <td>{{ \App\Models\User::find($transaction->fk_receiver)->firstname }}</td>
                        <td>{{ \App\Models\User::find($transaction->fk_sender)->firstname }}</td>
                        <td>{{ $transaction->description }}</td>
                        <td>{{ $transaction->reference }}</td>
                        <td>{{ $transaction->datetime }}</td>
                        <td>{{ (\App\Models\User::find($transaction->fk_receiver)->firstname == auth()->user()->firstname ? '+' : '-') . $transaction->amount }}€</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </main>
@endsection

