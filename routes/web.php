<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BankaccountController;
use App\Http\Controllers\TransactionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', function () { return view('dashboard');})->name('dashboard');

    Route::get('/test', [BankaccountController::class, 'generateIban']);
    Route::get('/transaction', [TransactionController::class, 'create'])->name('transaction');
    Route::post('/transaction', [TransactionController::class, 'store'])->name('newTransaction');
});

require __DIR__ . '/auth.php';
